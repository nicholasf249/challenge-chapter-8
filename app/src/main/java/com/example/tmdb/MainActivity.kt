package com.example.tmdb

import android.os.Bundle
import android.text.TextUtils
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.tmdb.navigation.AppNavigation
import com.example.tmdb.room.UserViewModel
import com.example.tmdb.ui.theme.TheMovieDBTheme
import com.example.tmdb.viewmodel.MovieViewModel
import com.example.tmdb.viewmodel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val searchViewModel: SearchViewModel = viewModel()
            val movieViewModel: MovieViewModel = viewModel()
            val vmod: UserViewModel = viewModel()
            TheMovieDBTheme {
                AppNavigation(searchViewModel, movieViewModel, vmod)
            }
        }
    }

}
fun String.isEmailValid(): Boolean {
    return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}