@file:OptIn(ExperimentalComposeUiApi::class)

package com.example.tmdb.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.tmdb.room.UserViewModel
import com.example.tmdb.screens.LoginScreen
import com.example.tmdb.screens.RegisterScreen
import com.example.tmdb.screens.*
import com.example.tmdb.viewmodel.MovieViewModel
import com.example.tmdb.viewmodel.SearchViewModel

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AppNavigation(searchViewModel: SearchViewModel, movieViewModel: MovieViewModel, vmod: UserViewModel) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = AppScreens.LoginScreen.name){
        composable(AppScreens.HomeScreen.name){
            HomeScreen(navController, movieViewModel)
        }
        composable(
            AppScreens.CategoryScreen.name+"/{category}",
            arguments = listOf(navArgument("category"){ type = NavType.StringType})
        ){
            CategoryScreen(navController, category = it.arguments?.getString("category"), movieViewModel)
        }
        composable(
            AppScreens.MovieScreen.name+"/{id}",
            arguments = listOf(navArgument("id"){ type = NavType.IntType})
        ){
            MovieScreen(navController, it.arguments?.getInt("id"), movieViewModel)
        }
        composable(
            AppScreens.SearchScreen.name
        ){
            SearchScreen(navController, searchViewModel)
        }
        composable(
            AppScreens.FavouritesScreen.name
        ){
            FavouritesScreen(navController, movieViewModel)
        }
        composable(
            AppScreens.LoginScreen.name
        ){
            LoginScreen(navController, vmod, modifier = Modifier )
        }
        composable(
                AppScreens.RegisterScreen.name
                ){
            RegisterScreen(navController, vmod, modifier = Modifier)
        }

    }
}