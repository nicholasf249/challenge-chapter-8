package com.example.tmdb.screens

import android.content.SharedPreferences
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Email
import androidx.compose.material.icons.outlined.Lock
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavHostController
import com.example.tmdb.R
import com.example.tmdb.isEmailValid
import com.example.tmdb.navigation.AppScreens
import com.example.tmdb.room.UserViewModel
import com.example.tmdb.viewmodel.SearchViewModel


import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@ExperimentalComposeUiApi
@Composable
fun LoginScreen(
    navController: NavHostController,
    vmod: UserViewModel,
    modifier: Modifier
) {

    var email by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()


    LazyColumn(
        modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        item {
            Text(
                text = "Login",
                modifier
                    .padding(top = 56.dp),
                fontSize = 20.sp,
                color = Color(0, 0, 0, 255)
            )

            Column(
                modifier
                    .padding(top = 150.dp)
                    .fillMaxSize(),
            ) {
                Column(
                    modifier
                        .padding(start = 16.dp, end = 16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                )
                {
                    OutlinedTextField(
                        value = email,
                        onValueChange = { typedEmail ->
                            email = typedEmail
                        },
                        modifier = modifier
                            .fillMaxWidth()
                            .padding(start = 20.dp, end = 20.dp, top = 15.dp, bottom = 10.dp),
                        singleLine = true,
                        label = { Text(text = "Masukkan Email") }
                    )
                }

                var passwordVisibility by remember {
                    mutableStateOf(false)
                }

                val passwordTrailingIcon = if (passwordVisibility)
                    painterResource(id = R.drawable.ic_view)
                else
                    painterResource(id = R.drawable.ic_hide)

                Column(
                    modifier
                        .padding(start = 16.dp, end = 16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    OutlinedTextField(
                        value = password,
                        onValueChange = { typedPassword ->
                            password = typedPassword
                        },
                        modifier = modifier
                            .fillMaxWidth()
                            .padding(start = 20.dp, end = 20.dp, top = 5.dp, bottom = 70.dp),
                        singleLine = true,
                        label = { Text(text = "Masukkan Password") },
                        trailingIcon = {
                            IconButton(
                                onClick = {
                                    passwordVisibility = !passwordVisibility
                                }
                            ) {
                                Icon(
                                    painter = passwordTrailingIcon,
                                    contentDescription = "Password Visibility"
                                )
                            }
                        },
                        visualTransformation = if (passwordVisibility) VisualTransformation.None
                        else
                            PasswordVisualTransformation()
                    )
                }
            }


            Column(
                modifier
                    .fillMaxWidth()
                    .height(265.dp),
            ) {

                Column(
                    modifier
                        .padding(start = 16.dp, end = 16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally

                ) {
                    Button(
                        onClick = {
                            coroutineScope.launch(Dispatchers.IO) {
                                val check = vmod.checkUserExists(
                                    email,
                                    password
                                )
                                coroutineScope.launch(Dispatchers.Main) {

                                    if (email.isEmpty()) {
                                        Toast.makeText(
                                            context,
                                            "Isi semua bagian!",
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                    }

                                    else if (check && password.isNotEmpty()) {
                                        navController.navigate(AppScreens.HomeScreen.name)
                                    } else {
                                        Toast.makeText(
                                            context,
                                            "Email atau Password Invalid!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }
                        },
                        modifier = modifier
                            .fillMaxWidth()
                            .height(75.dp)
                            .padding(start = 20.dp, end = 20.dp, top = 10.dp, bottom = 25.dp)
                    ) {
                        Text(
                            "LOGIN",
                            fontSize = 17.sp
                        )
                    }
                }

                ClickableText(
                    text = AnnotatedString("Belum punya akun?"),
                    style = MaterialTheme.typography.caption.copy(
                        color = Color.Blue
                    ),
                    modifier =
                    modifier
                        .padding(start = 0.dp, end = 0.dp)
                        .weight(1.5f)
                        .align(Alignment.CenterHorizontally),

                    onClick = {
                        navController.navigate(AppScreens.RegisterScreen.name)
                    }
                )
            }
        }
    }
}

