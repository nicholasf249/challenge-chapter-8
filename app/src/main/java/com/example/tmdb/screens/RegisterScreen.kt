package com.example.tmdb.screens

import android.app.Application
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.AccountBox
import androidx.compose.material.icons.outlined.Email
import androidx.compose.material.icons.outlined.Lock
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.tmdb.isEmailValid
import com.example.tmdb.navigation.AppScreens
import com.example.tmdb.R
import com.example.tmdb.room.UserViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@ExperimentalComposeUiApi
@Composable
fun RegisterScreen(
    navController: NavHostController,
    vmod: UserViewModel,
    modifier: Modifier

) {
    LazyColumn(
        Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item {
            Head()
            Column(
                modifier
                    .fillMaxSize(),
            ) {
                var name by rememberSaveable { mutableStateOf("") }
                var email by rememberSaveable { mutableStateOf("") }
                var password by rememberSaveable { mutableStateOf("") }
                var password2 by rememberSaveable { mutableStateOf("") }
                val coroutineScope = rememberCoroutineScope()
                val context = LocalContext.current

                Column(
                    modifier =
                    Modifier
                        .padding(start = 16.dp, end = 16.dp)
                        .clip(RoundedCornerShape(35.dp, 35.dp, 0.dp, 0.dp)),
                    horizontalAlignment = Alignment.CenterHorizontally

                ) {
                    Column(
                        modifier =
                        Modifier
                            .fillMaxWidth()
                            .padding(start = 20.dp, end = 20.dp, top = 5.dp, bottom = 10.dp),
                        horizontalAlignment = Alignment.CenterHorizontally

                    )
                    {
                        OutlinedTextField(
                            value = name,
                            onValueChange = {
                                name = it
                            },
                            label = { Text("Masukkan Username") },
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                    Column(
                        modifier =
                        Modifier
                            .fillMaxWidth()
                            .padding(start = 20.dp, end = 20.dp, bottom = 10.dp),
                        horizontalAlignment = Alignment.Start
                    ) {
                        OutlinedTextField(
                            value = email,
                            onValueChange = {
                                email = it
                            },

                            label = { Text(text = "Masukkan Email") },
                            modifier = Modifier.fillMaxWidth()
                        )


                        // Password visibility will remain intact when there is configuration changes
                        var passwordVisibility by remember {
                            mutableStateOf(false)
                        }
                        var passwordVisibility2 by remember {
                            mutableStateOf(false)
                        }

                        // Icon button for visibility of password
                        val passwordTrailingIcon = if (passwordVisibility)
                            painterResource(id = R.drawable.ic_view)
                        else
                            painterResource(id = R.drawable.ic_hide)

                        val passwordTrailingIcon2 = if (passwordVisibility2)
                            painterResource(id = R.drawable.ic_view)
                        else
                            painterResource(id = R.drawable.ic_hide)
                        OutlinedTextField(
                            value = password,
                            onValueChange = {
                                password = it
                            },
                            label = { Text("Masukkan Password") },
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 10.dp, bottom = 10.dp),
                            trailingIcon = {
                                IconButton(onClick = {
                                    passwordVisibility = !passwordVisibility
                                }) {
                                    Icon(
                                        painter = passwordTrailingIcon,
                                        contentDescription = "Password Visibility"
                                    )
                                }
                            },
                            visualTransformation =
                            if (passwordVisibility) VisualTransformation.None
                            else
                                PasswordVisualTransformation()
                        )
                        OutlinedTextField(
                            value = password2,
                            onValueChange = {
                                password2 = it
                            },

                            label = { Text("Masukkan Konfirmasi Password") },
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(bottom = 40.dp),
                            trailingIcon = {
                                IconButton(onClick = {
                                    passwordVisibility2 = !passwordVisibility2
                                }) {
                                    Icon(
                                        painter = passwordTrailingIcon2,
                                        contentDescription = "Password Visibility2"
                                    )
                                }
                            },
                            visualTransformation =
                            if (passwordVisibility2) VisualTransformation.None
                            else
                                PasswordVisualTransformation()
                        )
                    }
                    Column(
                        modifier
                            .fillMaxWidth()
                            .height(265.dp),
                    ) {

                        Column(
                            modifier
                                .fillMaxWidth()
                                .clip(RoundedCornerShape(0.dp, 0.dp, 35.dp, 35.dp)),
                            horizontalAlignment = Alignment.CenterHorizontally

                        ) {
                            Button(
                                onClick = {
                                    if (name.isEmpty() || email.isEmpty() || password.isEmpty() || password2.isEmpty()){
                                        Toast.makeText(context,"Data masih kosong!",Toast.LENGTH_SHORT).show()
                                    }

                                    else if (password == password2 && password.isNotEmpty()){
                                        coroutineScope.launch(Dispatchers.IO) {
                                            vmod.userProfile(
                                                name,
                                                "",
                                                "",
                                                "",
                                                email,
                                                password
                                            )}
                                        navController.navigate(AppScreens.LoginScreen.name)
                                    }

                                    else {Toast.makeText(context,"Password tidak sesuai!",Toast.LENGTH_SHORT).show()}},
                                modifier = modifier
                                    .fillMaxWidth()
                                    .height(75.dp)
                                    .padding(start = 20.dp, end = 20.dp, top = 10.dp, bottom = 25.dp
                                    )
                            ) {
                                Text(
                                    "Daftar",
                                    fontSize = 17.sp
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Head(modifier: Modifier = Modifier) {
    Box {
        Column(
            modifier
                .fillMaxWidth()
                .height(200.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {

        }
        Column(
            modifier
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = "Register",
                modifier
                    .padding(top = 56.dp),
                fontSize = 20.sp,
                color = Color(0, 0, 0, 255)
            )
        }
    }
}


