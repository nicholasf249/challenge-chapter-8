package com.example.tmdb.datastore

data class FavList(
    var idList: List<Int> = emptyList()
)
