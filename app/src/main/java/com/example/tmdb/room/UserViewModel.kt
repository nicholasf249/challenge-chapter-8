package com.example.tmdb.room

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.tmdb.model.Login
import com.example.tmdb.repository.MovieRepo
import com.example.tmdb.room.UserDao
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val userDao: UserDao) : ViewModel() {

    suspend fun userProfile(name: String, fname:String, date:String, adress:String, email: String, password: String) {
        val data = dataEntry(name,fname,date,adress, email, password)
        insertToDatabase(data)
    }
    private fun dataEntry(name: String,fname:String,date:String,adress:String, email: String, password: String): Login {
        return Login(
            id = null, name,fname,date,adress, email, password
        )
    }

    private suspend fun insertToDatabase(data: Login) {
        userDao.registerUser(data)
    }

    suspend fun checkUserExists(email: String, password: String): Boolean {
        return userDao.getUser(email, password)
    }

 suspend fun getUserName(email: String): String {
        return userDao.getName(email)
    }

}
