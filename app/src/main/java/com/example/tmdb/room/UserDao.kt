package com.example.tmdb.room

import androidx.room.*
import com.example.tmdb.model.Login

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun registerUser(userData: Login)

    @Query("Select Exists(Select * From Login Where email = :email And password = :password)")
    suspend fun getUser(email: String, password: String): Boolean

    @Query("Select * From Login Where email = :email")
    suspend fun getProfile(email: String?): Login

    @Query("Select name From Login Where email = :email")
    suspend fun getName(email: String): String

    @Update
    suspend fun updateData(updatedData: Login): Int
}