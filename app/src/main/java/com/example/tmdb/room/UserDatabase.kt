package com.example.tmdb.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.tmdb.model.Login

@Database(entities = [Login::class], version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {
    abstract fun daoLogin(): UserDao
}