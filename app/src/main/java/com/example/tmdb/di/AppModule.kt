package com.example.tmdb.di

import android.content.Context
import androidx.room.ProvidedTypeConverter
import androidx.room.Room
import androidx.room.TypeConverter
import com.example.tmdb.network.ApiService
import com.example.tmdb.repository.MovieRepo
import com.example.tmdb.repository.SearchRepo
import com.example.tmdb.room.MovieDao
import com.example.tmdb.room.MovieDatabase
import com.example.tmdb.room.UserDao
import com.example.tmdb.room.UserDatabase
import com.example.tmdb.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideSearchRepo(apiService: ApiService) = SearchRepo(apiService)

    @Singleton
    @Provides
    fun provideRoomDatabase(@ApplicationContext context: Context) : MovieDatabase{
        return Room.databaseBuilder(context,MovieDatabase::class.java,"movie_database")
            .fallbackToDestructiveMigration()
            .build()
    }
    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): UserDatabase {
        return Room.databaseBuilder(appContext, UserDatabase::class.java, "user_database")
            .build()
    }

    @Singleton
    @Provides
    fun provideMovieRepo(apiService: ApiService, movieDao: MovieDao, @ApplicationContext context: Context): MovieRepo {
        return MovieRepo(apiService, movieDao, context)
    }

    @Singleton
    @Provides
        fun provideUserData(userDatabase: UserDatabase): UserDao {
            return userDatabase.daoLogin()
        }


    @Singleton
    @Provides
    fun provideMoviesDao(movieDatabase: MovieDatabase): MovieDao{
        return movieDatabase.movieDao()
    }

    @Singleton
    @Provides
    fun provideApiService() : ApiService{
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
}