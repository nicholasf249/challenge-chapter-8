package com.example.tmdb.util

object Constants {
    const val API_KEY = "3f4e6c7a45f1cac5b12c889c1f53185a"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original"
    //search eg
    //https://api.themoviedb.org/3/search/movie?api_key=251d1bb4f29db6dfde5923cf5117b0c2&language=en-US&query=a&page=1&include_adult=false

}
