package com.example.tmdb.model

data class Genre(
    val id: Int,
    val name: String
)