package com.example.tmdb.model

data class Keyword(
    val id: Int,
    val name: String
)