package com.example.tmdb.model

data class Credits(
    val cast: List<Cast>? = null,
    val id: Int? = 0
)